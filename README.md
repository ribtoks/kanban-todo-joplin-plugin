# TODO.md Kanban Plugin for Joplin

## About

This plugin shows a document in [TODO.md syntax](https://github.com/todomd/todo.md) as a kanban board inside Joplin.

## Documentation

This plugin also has a Markdown-it extension. You can find Markdown-it architecture [here](https://github.com/markdown-it/markdown-it/blob/master/docs/architecture.md) and markdown tokens [here](https://github.com/markdown-it/markdown-it/blob/master/lib/token.js). The most useful thing for debugging Markdown-it was [the playground](https://markdown-it.github.io/).

## Developing

### Setup

```
npm install -g webpack
npm install -g webpack-cli
npm install -g yo generator-joplin

# Extra:
# yo joplin --update
```

### Run

Execute in the repository root: `npm run dist && ~/Applications/Joplin-1.5.7.AppImage --env dev`

## Building the plugin

The plugin is built using Webpack, which creates the compiled code in `/dist`. A JPL archive will also be created at the root, which can use to distribute the plugin.

To build the plugin, simply run `npm run dist`.

The project is setup to use TypeScript, although you can change the configuration to use plain JavaScript.

## Updating the plugin framework

To update the plugin framework, run `yo joplin --update`

Keep in mind that doing so will overwrite all the framework-related files **outside of the "src/" directory** (your source code will not be touched). So if you have modified any of the framework-related files, such as package.json or .gitignore, make sure your code is under version control so that you can check the diff and re-apply your changes.

For that reason, it's generally best not to change any of the framework files or to do so in a way that minimises the number of changes. For example, if you want to modify the Webpack config, create a new separate JavaScript file and include it in webpack.config.js. That way, when you update, you only have to restore the line that include your file.