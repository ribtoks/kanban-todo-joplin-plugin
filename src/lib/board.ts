export class Board {
    noteBody: string;

    constructor(body: string) {
        this.noteBody = body;
    }

    moveCard(cardId: number, columnId: number, markDone: boolean): string {
        let columnIndex = -1;
        let cardIndex = -1;
        let lineIndexFrom = -1;
        let cardBody = '';
        let columnToLine = new Map();
    
        const lines = this.noteBody.split('\n');
        const linesLength = lines.length;
    
        for (let i = 0; i < linesLength; i++) {
            const line = lines[i];
    
            const columnMatch = line.match(/^(#+)\s(.*)/);
            if (columnMatch) {
                const level = columnMatch[1].length;
                if (level == 2) {
                    columnIndex++;
                    columnToLine.set(columnIndex, i);
                }
    
                continue;
            }
    
            const cardMatch = line.match(/-\s\[(\ |x)\](.*)/);
            if (cardMatch) {
                cardIndex++;
    
                if (cardIndex == cardId) {
                    lineIndexFrom = i;
                    cardBody = cardMatch[2].trim();
                }
            }
        }
    
        if (!columnToLine.has(columnId) ||
            (lineIndexFrom == -1)) {
            return this.noteBody;
        }
    
        let lineIndexTo = columnToLine.get(columnId);
        // remove card from previous place
        lines.splice(lineIndexFrom, /*items to delete*/ 1);
        // insert card to new column
        // if we insert after our card, need to account removal
        if (lineIndexTo > lineIndexFrom) {
            lineIndexTo--;
        }

        while ((lineIndexTo < linesLength - 1) && (lines[lineIndexTo + 1].match(/^\s*$/) !== null)) { lineIndexTo++; }

        const insertedCard = markDone
            ? ('- [x] ' + cardBody)
            : ('- [ ] ' + cardBody);
        lines.splice(lineIndexTo + 1, /*items to delete*/ 0, insertedCard)
        return lines.join('\n');
    }
}