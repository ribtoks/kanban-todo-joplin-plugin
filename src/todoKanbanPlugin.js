const LAST_CARD_ATTR = 'last-card';
const CARDS_COUNT_ATTR = 'cards-count';
const COLUMN_INDEX_ATTR = 'column-index';
const CARD_ID_ATTR = 'card-id';

function cardTitle(content) {
    var length = content.length;
    var i = 0;
    // we assume all done cards ([x]) are in the "Done" dolumn
    while ((i < length) && (
        (content[i] == ' ') ||
        (content[i] == '[') ||
        (content[i] == ']'))) { i++; }
    
    return content.substring(i);
}

function todoKanbanPlugin(markdownIt, options) {
    var columnsCount = 0;

    markdownIt.core.ruler.push('processColumns', function (state) {
        var tokens = state.tokens;
        var columnIndex = 0;
        var columnCardsCount = 0;
        var cardsCount = 0;
        var lastCardToken = null;
        var lastColumnToken = null;

        for (var i = 1; i < tokens.length; i++) {
            var token = tokens[i];

            if (token.type == 'list_item_open') {
                lastCardToken = token;
                token.attrSet(CARD_ID_ATTR, cardsCount);
                token.attrSet(COLUMN_INDEX_ATTR, columnIndex);
                columnCardsCount++;
                cardsCount++;
            }

            // currently we only turn ## headings into columns
            if ((token.type === 'heading_open') && (token.markup == '##')) {
                if (lastCardToken) {
                    lastCardToken.attrSet(LAST_CARD_ATTR, 1);
                }

                if (lastColumnToken) {
                    lastColumnToken.attrSet(CARDS_COUNT_ATTR, columnCardsCount);
                }

                token.attrSet(COLUMN_INDEX_ATTR, columnIndex);
                columnIndex++;
                columnCardsCount = 0;
                lastCardToken = null;
                lastColumnToken = token;
            }
        }

        if (lastCardToken) {
            lastCardToken.attrSet(LAST_CARD_ATTR, 1);
        }

        if (lastColumnToken) {
            lastColumnToken.attrSet(CARDS_COUNT_ATTR, columnCardsCount);
        }

        columnsCount = columnIndex;
    });

    markdownIt.renderer.rules.heading_open = function (tokens, idx, opts, env, self) {
        var content = '';
        if (idx >= tokens.length) { return content; }

        var children = tokens[idx + 1].children;
        var token = tokens[idx];
        if ((token.markup == '##') && children && children.length) {
            var widthPercent = 100 / columnsCount - 2;
            widthPercent = Math.floor(widthPercent * 100) / 100

            // TODO: we actually need to render the contents instead of the plain text
            var title = children[0].content;
            content += '<div class="kanban-column" style="width:' + widthPercent + '%;">\n' +
                '<div class="kanban-column-title">' + title + '</div>\n';

            // column with no cards
            if (token.attrGet(CARDS_COUNT_ATTR) == 0) {
                content += '</div>\n';
            }
        }

        return content;
    }

    markdownIt.renderer.rules.list_item_open = function (tokens, idx, opts, env, self) {
        var content = '';
        if (idx >= tokens.length) { return content; }

        if ((tokens[idx + 1].type == 'paragraph_open') &&
            (tokens[idx + 2].type == 'inline')) {
            // TODO: we actually need to render the contents before displaying
            var title = cardTitle(tokens[idx + 2].content);
            var cardId = tokens[idx].attrGet(CARD_ID_ATTR);
            var columnIndex = columnsCount - 1;

            const postMessageWithResponseTest = `
						webviewApi.postMessage('todoKanbanPlugin', {'card_id':${cardId}, 'column_id':${columnIndex}});
						return false;
					`;

            content = `<div class="kanban-card">${title} 
                <a href="#" class="kanban-done-button" cardid="${cardId}" 
                    onclick="${postMessageWithResponseTest.replace(/\n/g, ' ')}">&#10003;</a>
                </div>\n`;
        }

        if (tokens[idx].attrGet(LAST_CARD_ATTR) == 1) {
            // close the column
            content += '</div>\n';
        }

        return content;
    }

    markdownIt.renderer.rules.list_item_close = function (tokens, idx, opts, env, self) { return ''; }
    markdownIt.renderer.rules.heading_close = function (tokens, idx, opts, env, self) { return ''; }
    markdownIt.renderer.rules.paragraph_open = function (tokens, idx, opts, env, self) { return ''; }
    markdownIt.renderer.rules.paragraph_close = function (tokens, idx, opts, env, self) { return ''; }
    markdownIt.renderer.rules.inline = function (tokens, idx, opts, env, self) { return ''; }
    markdownIt.renderer.rules.text = function (tokens, idx, opts, env, self) { return ''; }
    markdownIt.renderer.rules.bullet_list_open = function (tokens, idx, opts, env, self) { return ''; }
    markdownIt.renderer.rules.bullet_list_close = function (tokens, idx, opts, env, self) { return ''; }

    // fixes for Joplin code

    markdownIt.renderer.rules.checkbox_wrapper_open = function (tokens, idx, opts, env, self) { return ''; }
    markdownIt.renderer.rules.checkbox_wrapper_close = function (tokens, idx, opts, env, self) { return ''; }
    markdownIt.renderer.rules.checkbox_input = function (tokens, idx, opts, env, self) { return ''; }
    markdownIt.renderer.rules.label_open = function (tokens, idx, opts, env, self) { return ''; }
    markdownIt.renderer.rules.label_close = function (tokens, idx, opts, env, self) { return ''; }
}

module.exports = {
    default: function (context) {
        return {
            plugin: todoKanbanPlugin,
            assets: function() {
                return [
                    { name: 'todoKanban.css'}
                ];
            }
        };
    },
}