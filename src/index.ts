import joplin from 'api';
import { ContentScriptType, MenuItemLocation } from 'api/types';
import { Board } from './lib/board'

joplin.plugins.register({
    onStart: async function () {
        console.info('TODO.md Kanban started!');

        await joplin.plugins.registerContentScript(
            ContentScriptType.MarkdownItPlugin,
            'todoKanbanPlugin',
            './todoKanbanPlugin.js'
        );

        await joplin.contentScripts.onMessage('todoKanbanPlugin', async (message:any) => {
            console.log('card_id: ' + message.card_id + ' column_id: ' + message.column_id);
            const note = await joplin.workspace.selectedNote();
            if (!note) {
                alert("Please select a note");
                return;
            }
            const board = new Board(note.body);
            const updatedBody = board.moveCard(message.card_id, message.column_id, false /*mark done*/);
            await joplin.commands.execute("editor.setText", updatedBody);
            //await joplin.data.put(['notes', note.id], null, { body: updatedBody });

			return message + '+response';
		});
    },
});
