import { Board } from "../lib/board"

const SAMPLE_1 = `
## Column 1

- [ ] Task 1
- [ ] Task 2

## Column 2

- [ ] Task 3
- [ ] Task 4

## Column 3

- [ ] Task 5
- [ ] Task 6
`;

const MOVED_1 = `
## Column 1

- [ ] Task 2

## Column 2

- [ ] Task 3
- [ ] Task 4

## Column 3

- [ ] Task 1
- [ ] Task 5
- [ ] Task 6
`;

const MOVED_2 = `
## Column 1

- [ ] Task 4
- [ ] Task 1
- [ ] Task 2

## Column 2

- [ ] Task 3

## Column 3

- [ ] Task 5
- [ ] Task 6
`;

test('Move card to Done', () => {
    const board = new Board(SAMPLE_1);
    var moved = board.moveCard(0, 2, false);
    expect(moved).toBe(MOVED_1);
});


test('Move card backward', () => {
    const board = new Board(SAMPLE_1);
    var moved = board.moveCard(3, 0, false);
    expect(moved).toBe(MOVED_2);
});